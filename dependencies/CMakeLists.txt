# Thomas Hahn, 2020
#
# Project: cmake-deps
# File: CMakeLists.txt file to handle the dependencies

# module GetDependencies
include(GetDependencies)

# output information
message(STATUS "Preparing dependencies...")

# dep library
if(BUILD_DEP)
    fetch_from_git(
        CONTENT_NAME dep-lib
        GIT_REPOSITORY https://thoemi01@bitbucket.org/thoemi01/dep.git
        GIT_TAG origin/master
        SOURCE_DIRECTORY ${SOURCE_DEP})
else()
    # specify dep_DIR to help find the package
    message(STATUS "---------------------")
    message(STATUS "Finding Dep ...")
    find_package(Dep REQUIRED)
    set_target_properties(dep::dep PROPERTIES IMPORTED_GLOBAL TRUE)
    if (TARGET dep::dep)
        message(STATUS "Dep found: Dep_DIR = ${Dep_DIR}")
    endif()
    message(STATUS "---------------------")
endif()

# output information
message(STATUS "Dependencies are prepared")
